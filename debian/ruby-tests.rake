require 'gem2deb/rake/testtask'

skiplist = File.read("debian/ruby-tests.skip").split
ENV["TESTOPTS"] = skiplist.map { |item| "--ignore-name='#{item}'" }.join(" ")
ENV["TESTOPTS"] += " --ignore-testcase=TestGemPlatform" if RUBY_VERSION < "3.2"
ENV["TESTOPTS"] += " --verbose"

exclude = %w[
  test/rubygems/test_gem_commands_update_command.rb
]

Rake::TestTask.new(:default) do |t|
  t.ruby_opts = %w[-w]
  t.libs << "test"
  t.test_files = FileList["test/**/test_*.rb"] - exclude
end
